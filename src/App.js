import "./App.scss";
import Navbar from "./Components/Navbar/Navbar";
import "antd/dist/antd.css";
import Home from "./Components/Home/Home";
import Loader from "./Components/Loader/Loader";
import { BackTop, ConfigProvider } from "antd";
import "./Languange/i18n";
import About from "./Components/About/About";
import Service from "./Components/Service/Service";
import WhyUs from "./Components/About/WhyUs";
import Answer from "./Components/Answers/Answer";
import Contact from "./Components/Contact/Contact";
import Portfolio from "./Components/Portfolio/Portfolio";
import Client from "./Components/Client/Client";
// import { ConfigProvider } from "antd";
import 'antd/dist/antd.variable.min.css'

ConfigProvider.config({
  theme: {
    primaryColor: "rgb(155, 33, 109)",
  },
});
function App() {
  return (
    <div className="App">
      <Loader />
      <Navbar />
      <Home />
      <About />
      <WhyUs />
      <BackTop />
      <Answer />
      <Service />
      <Portfolio />
      <Client />
      <Contact />
    </div>
  );
}

export default App;
