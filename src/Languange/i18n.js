import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { langs } from './lang';
import { localStorageKeys } from './localStorageKeys';
import ru from './ru.json';
import uz from './uz.json';

const resources = {
  uz: {
    translation: uz,
  },
  ru: {
    translation: ru,
  },
};

const defaultLang = localStorage.getItem(localStorageKeys.LANG) ?? langs.RU

i18n.use(initReactI18next).init({
  resources,
  lng: defaultLang,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
