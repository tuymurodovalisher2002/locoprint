import React from "react";
import "./Contact.scss";
import "../Client/Responsive.scss";

function Contact() {
  return (
    <>
      <section className="contact" id="contact">
        <div className="container">
          <h2>Контакты</h2>
          <div className="row">
            <div className="col-12 col-md-3 col-sm-3 col-lg-3">
              <div className="contact-info">
                <h5>Адрес:</h5>
                <p>
                  <span>г. Навои, Узбекистан </span>
                  <span>Навоийский р-н,ул. Навруз</span>
                  <span>Оказываем услуги по всему Узбекистану</span>
                </p>
              </div>
            </div>
            <div className="col-12 col-md-3 col-sm-3 col-lg-3">
              <div className="contact-info">
                <h5>Почта:</h5>
                <p style={{ textDecoration: "underline" }}>
                  <a href="https://t.me/lokoprint">info@lokoprint.uz</a>
                </p>
              </div>
            </div>
            <div className="col-12 col-md-3 col-sm-3 col-lg-3">
              <div className="contact-info">
                <h5>Телефон:</h5>
                <p>+998 99 339 90 00</p>
              </div>
            </div>
            <div className="col-12 col-3 col-sm-3 col-lg-3">
              <div className="contact-link">
                <h5>О компании</h5>
                <a href="#service">Услуги</a>
                <a href="#portfolio">Портфолио</a>
                <a href="#client">Клиенты</a>
                <a href="#about">Информация о компании</a>
              </div>
            </div>
          </div>
          <div className="footer">
            <p>LokoPrint.uz | All Rights Reserved | Powered by Softex.uz</p>
          </div>
        </div>
      </section>
      <div className="line" style={{ margin: 0 }}></div>
    </>
  );
}

export default Contact;
