import React from "react";
import "./About.scss";
import Timer from "./WhyUsIcons/Timer";
import Money from "./WhyUsIcons/Money";
import Printer from "./WhyUsIcons/Printer";
import { Fade } from "react-reveal";
import "./Responsive.scss";

function WhyUs() {
  return (
    <section className="whyUs">
      <div className="container">
        <div className="row justify-content-center align-items-center">
          <div className="col-12 col-md-4 col-sm-4 col-lg-4 jcCenter">
            <Fade left>
              <div className="whyCard">
                <Timer />
                <h3>Короткие сроки</h3>
                <p>
                  Срок выполнения заказов — от нескольких часов до суток. Всегда
                  на складе запас бумаги разных сортов и плотностей. Время
                  выполнения типового заказа — 24 часа.
                </p>
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-4 col-sm-4 col-lg-4 jcCenter">
            <Fade bottom>
              <div className="whyCard wCardActive">
                <Printer />
                <h3>Новейшее оборудование</h3>
                <p>
                  Для производства полиграфии в нашей типографии используется
                  современное оборудование ведущих мировых производителей как
                  Heidelberg, Konica Minolta, Horizon.
                </p>
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-4 col-sm-4 col-lg-4 jcCenter">
            <Fade right>
              <div className="whyCard">
                <Money />
                <h3>Выгодные цены</h3>
                <p>
                  Имея полностью оборудованные цеха для печати полиграфии в
                  Ташкенте, компания print.uz обеспечивает своих заказчиков
                  высококачественной полиграфией по лучшим ценам.
                </p>
              </div>
            </Fade>
          </div>
        </div>
      </div>
      <div className="line"></div>
    </section>
  );
}

export default WhyUs;
