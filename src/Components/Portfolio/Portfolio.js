import { Image } from "antd";
import React, { useState } from "react";
import "./Portfolio.scss";
import { Button } from "antd";
import RefreshIcon from "./RefreshIcon";
import { Fade } from "react-reveal";
import img1 from "../../Images/1.png";
import img2 from "../../Images/2.png";
import img3 from "../../Images/3.png";
import img4 from "../../Images/4.png";
import img5 from "../../Images/5.png";
import img6 from "../../Images/6.png";
import img7 from "../../Images/7.png";
import img8 from "../../Images/8.png";
import img9 from "../../Images/9.png";
import img10 from "../../Images/10.png";
import img11 from "../../Images/11.png";
import img12 from "../../Images/12.png";
import img13 from "../../Images/13.png";
import "./Responsive.scss";

function Portfolio() {
  const [clickBtn, setClickBtn] = useState(false);
  const [btnClass, setBtnClass] = useState(false);
  let count = 0;
  const nextImg = () => {
    count++;
    setClickBtn(true);
    setBtnClass(true);
    // if (count === 2) {
    //   setClickBtnSecond(true);
    // }
  };
  return (
    <section className="portfolio" id="portfolio">
      <div className="container">
        <div className="portfolio-title">
          <Fade left>
            <h5>Наши работы</h5>
          </Fade>
          <Fade right delay={100}>
            <h1>Портфолио</h1>
          </Fade>
        </div>
        <div className="portfolio-text">
          <Fade top delay={150}>
            <span>
              Наша Компания поставила перед собой грандиозные планы. Каждое
              достижение на этом пути очень важно для нас.
            </span>{" "}
          </Fade>
          <Fade top delay={200}>
            <span>Поэтому мы хотим поделиться с Вами.</span>
          </Fade>
        </div>
        <div className="row jcEvenly">
          <div className="col-12 col-md-6 col-sm-6 col-lg-3 jcEvenly">
            <Fade bottom delay={150}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img10}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={200}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img11}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={250}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img12}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={300}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img13}
                />
              </div>
            </Fade>
          </div>
        </div>
        <div className={clickBtn ? "row jcEvenly" : "dn"}>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={150}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img4}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={200}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img5}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={250}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img6}
                />
              </div>
            </Fade>
          </div>
          {/* <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={300}>
              <div className="portfolio-img">
                <Image preview={{ mask: "Ko'rish" }} src={img13} />
              </div>
            </Fade>
          </div> */}
        </div>
        <div className={clickBtn ? "row jcEvenly" : "dn"}>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={150}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img1}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={200}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img2}
                />
              </div>
            </Fade>
          </div>
          <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={250}>
              <div className="portfolio-img">
                <Image
                  preview={{ mask: "Нажмите, чтобы просмотреть" }}
                  src={img3}
                />
              </div>
            </Fade>
          </div>
          {/* <div className="col-12 col-md-3 col-sm-3 col-lg-3 jcEvenly">
            <Fade bottom delay={300}>
              <div className="portfolio-img">
                <Image preview={{ mask: "Ko'rish" }} src={img13} />
              </div>
            </Fade>
          </div> */}
        </div>
        <div className={btnClass ? "dn" : "portfolio-btn"}>
          <Fade left delay={100}>
            <Button onClick={nextImg} className={btnClass ? "dn" : "nextBtn"}>
              Загрузить еще <RefreshIcon />
            </Button>
          </Fade>
        </div>
      </div>
    </section>
  );
}

export default Portfolio;
