import React from "react";
import "./Answer.scss";
import { Collapse } from "antd";
import AnswerPlus from "./AnswerIcon/AnswerPlus";
import AnswerMinus from "./AnswerIcon/AnswerMinus";
import { Fade } from "react-reveal";
import "./Responsive.scss";

const { Panel } = Collapse;
function Answer() {
  const text1 = (
    <p>
      Мы думаем, потому что, мы <sapn>заботимся</sapn> о наших клиентах.
      Оказывая полный спектр <span>полиграфических</span> и{" "}
      <span>дизайнерских</span> услуг, выполняя заказы в кратчайшие сроки, мы
      стремимся к удовлетворению каждой потребности <span>клиента</span>.
    </p>
  );
  const text2 = (
    <p>
      Сегодня сроки выполнения заказа играют очень важную роль! Компания {" "}
      <span>Lokoprint.uz</span> уделяет особое внимание качеству и срокам
      выполнения заказа, ведь для компании важна любая потребность клиента.{" "}
      <span>Компания</span> выполняет заказы оперативно. В зависимости от{" "}
      <span>количества</span> и <span>сложности</span> работ сроки выполнения
      могут достигать 24 часов.
    </p>
  );
  const text3 = (
    <p>
      В производстве мы используем только современное оборудование известных
      мировых производителей. Такое оборудование позволяет в кратчайшие сроки
      выполнять большой объём работ без потери качества производимой продукции.
    </p>
  );
  const header1 = (
    <h4>
      Почему клиенты выбирают <span>Lokoprint.uz ?</span>
    </h4>
  );
  const header2 = (
    <h4>
      <span>Быстро</span> сделать получится ?
    </h4>
  );
  const header3 = (
    <h4>
      Какое оборудование вы <span>используете ?</span>{" "}
    </h4>
  );
  return (
    <section className="answer">
      <div className="container-fluid">
        <div className="row alignCenter">
          <div className="col-12 col-md-6 col-sm-6 col-lg-6 jcCenter">
            <Fade bottom>
              <div className="answer_img"></div>
            </Fade>
          </div>
          <div className="col-12 col-md-6 col-sm-6 col-lg-6 jcCenter">
            <div className="answer-question">
              <Fade bottom>
                <h5>Вопрос-ответ</h5>
              </Fade>
              <Fade bottom delay={100}>
                <h2>Преимущества</h2>
              </Fade>
              <Fade bottom delay={150}>
                <Collapse
                  bordered={false}
                  defaultActiveKey={["1"]}
                  className="ans-collapse"
                  expandIcon={({ isActive }) =>
                    isActive ? <AnswerMinus /> : <AnswerPlus />
                  }
                  style={{ background: "transparent" }}
                >
                  <Panel header={header1} key="1" className="ans-panel">
                    {text1}
                  </Panel>
                  <Panel header={header2} key="2" className="ans-panel">
                    {text2}
                  </Panel>
                  <Panel header={header3} key="3" className="ans-panel">
                    {text3}
                  </Panel>
                </Collapse>
              </Fade>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Answer;
