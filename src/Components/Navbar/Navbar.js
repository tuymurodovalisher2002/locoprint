import { Dropdown, Menu, Button, Drawer } from "antd";
import MenuItem from "antd/lib/menu/MenuItem";
import React, { useState } from "react";
import { Fade } from "react-reveal";
import "./Navbar.scss";
import { useTranslation } from "react-i18next";
import logo from "../../Images/logo.png";
import { langs } from "../../Languange/lang";
import "./Responsive.scss";
import NavbarBurger from "./NavbarBurger";

function Navbar() {
  const { t, i18n } = useTranslation();
  const ruLang = (lang) => {
    i18n.changeLanguage(lang);
  };
  const uzLang = (lang) => {
    i18n.changeLanguage(lang);
  };
  const [langBtn, setLangBtn] = useState(false);
  const lang = () => {
    setLangBtn(!langBtn);
    if (langBtn) {
      i18n.changeLanguage("uz");
    } else {
      i18n.changeLanguage("ru");
    }
  };
  // const menu = (
  //   <Menu>
  //     <MenuItem>
  //       <a
  //         target="_blank"
  //         rel="noopener noreferrer"
  //         style={{ color: "var(--bnfText)" }}
  //         onClick={() => uzLang("uz")}
  //       >
  //         O'zbek
  //       </a>
  //     </MenuItem>
  //     <MenuItem>
  //       <a
  //         target="_blank"
  //         rel="noopener noreferrer"
  //         style={{ color: "var(--bnfText)" }}
  //         onClick={() => ruLang("ru")}
  //       >
  //         Русский
  //       </a>
  //     </MenuItem>
  //   </Menu>
  // );
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };
  return (
    <Fade top cascade delay={300}>
      <nav className="navbar">
        <div className="container">
          <div className="nav-element jcBetween alignCenter">
            <a className="nav-brand" href="#">
              <img src={logo} alt="logo" />
            </a>
            <div className="nav-link">
              <a href="#about" className="jcCenter alignCenter">
                О компании
              </a>
              <a href="#service" className="jcCenter alignCenter">
                Услуги
              </a>
              <a href="#portfolio" className="jcCenter alignCenter">
                Портфолио
              </a>
              <a href="#contact" className="jcCenter alignCenter">
                Контакты
              </a>
              <a href="#client" className="jcCenter alignCenter">
                Клиент
              </a>
            </div>
            {/* <Dropdown overlay={menu} placement="bottom" className="langBtn">
              <Button className="langBtn">{t("navbar.lang")}</Button>
            </Dropdown> */}
            <div className="mobileLink">
              <Button onClick={showDrawer}>
                <NavbarBurger />
              </Button>
              <Drawer
                title="Locoprint.uz"
                placement="right"
                onClose={onClose}
                visible={visible}
              >
                <a href="#about" onClick={onClose}>
                  О компании
                </a>
                <a href="#service" onClick={onClose}>
                  Услуги
                </a>
                <a href="#portfolio" onClick={onClose}>
                  Портфолио
                </a>
                <a href="#contact" onClick={onClose}>
                  Контакты
                </a>
                <a href="#client" onClick={onClose}>
                  Клиент
                </a>
                {/* <Dropdown overlay={menu} placement="bottom">
                  <Button>{t("navbar.lang")}</Button>
                </Dropdown> */}
                {/* <Button onClick={lang}>{t("navbar.lang")}</Button> */}
              </Drawer>
            </div>
          </div>
        </div>
      </nav>
    </Fade>
  );
}

export default Navbar;
