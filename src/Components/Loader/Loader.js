import React, { useState } from "react";
import { Spin } from "antd";
import "./Loader.scss";

function Loader() {
  const [loader, setLoader] = useState(false);
  setTimeout(() => {
    setLoader(true);
  }, 800);
  return (
    <div className={loader ? "loadNot" : "loader jcCenter alignCenter"}>
      <Spin size="large" />
    </div>
  );
}

export default Loader;
