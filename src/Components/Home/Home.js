import React from "react";
import "./Home.scss";
import { Fade } from "react-reveal";
import { Button } from "antd";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectFade, Autoplay, Pagination, EffectCreative } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/effect-fade";
import "swiper/css/effect-creative";
import { useTranslation } from "react-i18next";
import "./Responsive.scss";

function Home() {
  const { t } = useTranslation();

  return (
    <section className="home">
      <div className="container-fluid">
        <Fade top delay={100}>
          <div className="home-line"></div>
        </Fade>
        <Swiper
          style={{ paddingBottom: 0 }}
          pagination={true}
          effect={"fade"}
          // creativeEffect={{
          //   prev: {
          //     shadow: true,
          //     translate: [0, 0, -1400],
          //   },
          //   next: {
          //     translate: ["100%", 0, 0],
          //   },
          // }}
          modules={[EffectFade, Autoplay, Pagination, EffectCreative]}
          className="mySwiper"
          autoplay={{
            delay: 5000,
            disableOnInteraction: false
          }}
          speed={1000}
          mousewheel={false}
        >
          <SwiperSlide>
            <section className="home-slider h_slide1">
              <div className="h_whiteBg jcAround alignCenter ">
                <div className="home-info">
                  <Fade left>
                    <h5>Привет!</h5>
                  </Fade>
                  <h1>
                    <Fade right>
                      <span>Хотите увеличить продажи </span>
                      <span>своего продукта? Тогда </span>
                    </Fade>
                    <Fade right delay={100}>
                      <span>обратите внимание,</span>
                    </Fade>
                    <Fade right delay={200}>
                      <span>на современность </span>
                      <span>вашей упаковки.</span>
                    </Fade>
                  </h1>
                  <p>
                    <Fade bottom delay={400}>
                      <span>{t("home.homeText1")}</span>
                    </Fade>
                    <Fade bottom delay={500}>
                      <span>{t("home.homeText2")}</span>
                    </Fade>
                  </p>
                  <Fade right delay={100}>
                    <Button>
                      <a href="#contact">Связаться с нами</a>
                    </Button>
                  </Fade>
                </div>
                <Fade right>
                  <div className="home-img"></div>
                </Fade>
              </div>
            </section>
          </SwiperSlide>
          <SwiperSlide>
            <section className="home-slider h_slide1">
              <div className="h_whiteBg jcAround alignCenter ">
                <div className="home-info">
                  <Fade left>
                    <h5>О компании!</h5>
                  </Fade>
                  <h1>
                    <Fade right>
                      <span>Оказываем полный</span>
                    </Fade>
                    <Fade right delay={100}>
                      <span>спектр дизайнерских</span>
                    </Fade>
                    <Fade right delay={200}>
                      <span>услуг. </span>
                    </Fade>
                  </h1>
                  <p>
                    <Fade bottom delay={400}>
                      <span>Закажите у Нас современную упаковку!</span>
                    </Fade>
                    {/* <Fade bottom delay={500}>
                      <span>
                        locoprint.uz — полиграфия в Навои, которой доверяют.
                      </span>
                    </Fade> */}
                  </p>
                  <Fade right delay={100}>
                    <Button>
                      <a href="#contact">Связаться с нами</a>
                    </Button>
                  </Fade>
                </div>
                <div className="home-img home-img2"></div>
              </div>
            </section>
          </SwiperSlide>
        </Swiper>
      </div>
    </section>
  );
}

export default Home;
