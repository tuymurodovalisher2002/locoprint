import { Button } from "antd";
import React from "react";
import TelegramIcon from "./TelegramIcon";
import "./Client.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
// import logo1 from "../../Images/client1.png";
// import logo2 from "../../Images/client2.png";
import logo3 from "../../Images/client3.png";
import logo4 from "../../Images/client4.png";
import logo5 from "../../Images/Sofin.png"
import logo from "../../Images/logo.png";
import softex from "../../Images/softex.png";
import { Autoplay, Pagination } from "swiper";
import { Fade } from "react-reveal";
import "./Responsive.scss";

function Client() {
  return (
    <>
      <section className="telegram">
        <div className="t-bg">
          <div className="container jcCenter alignCenter">
            <Fade bottom>
              <div className="t-card jcEvenly alignCenter">
                <p>Подпишитесь на наш</p>
                <h2 className="alignCenter">
                  Telegram-канал
                  <TelegramIcon />
                </h2>
                <Button>
                  <a href="https://t.me/lokoprint">Подписаться на канал</a>
                </Button>
              </div>
            </Fade>
          </div>
        </div>
      </section>
      <section className="client" id="client">
        <div className="container">
          <div className="client-title">
            <Fade right>
              <h5>Клиенты</h5>
            </Fade>
            <Fade left delay={100}>
              <h1>Нам доверяют</h1>
            </Fade>
          </div>
          <div className="client-info">
            <Fade left delay={150}>
              <p>
                На всех этапах выполнения заказа мы прислушиваемся к мнению
                клиента и предлагаем решения, исходя из многолетнего
                профессионального опыта, заложенного бюджета и срочности
                выполнения.{" "}
              </p>
            </Fade>
          </div>
          <div className="client-logos">
            <Swiper
              style={{
                paddingBottom: "50px"
              }}
              // slidesPerView={5}
              spaceBetween={30}
              autoplay={{
                delay: 5000,
                disableOnInteraction: false
              }}
              pagination={{
                clickable: true
              }}
              breakpoints={{
                300: { slidesPerView: 1 },
                768: { slidesPerView: 5 }
              }}
              modules={[Autoplay, Pagination]}
              className="mySwiper"
            >
              {/* <Fade bottom delay={100}>
                <SwiperSlide>
                  <div className="clientImg">
                    <img alt="img" src={logo1} />
                  </div>
                </SwiperSlide>
              </Fade> */}
              <Fade bottom delay={130}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={logo3} />
                  </div>
                </SwiperSlide>
              </Fade>
              <Fade bottom delay={160}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={logo4} />
                  </div>
                </SwiperSlide>
              </Fade>
              {/* <Fade bottom delay={190}>
                <SwiperSlide>
                  <div className="clientImg">
                    <img alt="img" src={logo1} />
                  </div>
                </SwiperSlide>
              </Fade> */}
              <Fade bottom delay={210}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={logo5} />
                  </div>
                </SwiperSlide>
              </Fade>
              <Fade bottom delay={240}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={logo} />
                  </div>
                </SwiperSlide>
              </Fade>
              {/* <Fade bottom delay={270}>
                <SwiperSlide>
                  <div className="clientImg">
                    <img alt="img" src={logo1} />
                  </div>
                </SwiperSlide>
              </Fade> */}
              <Fade bottom delay={300}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={softex} />
                  </div>
                </SwiperSlide>
              </Fade>
              {/* <Fade bottom delay={100}>
                <SwiperSlide
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <div className="clientImg">
                    <img alt="img" src={logo} />
                  </div>
                </SwiperSlide>
              </Fade> */}
            </Swiper>
          </div>
        </div>
      </section>
    </>
  );
}

export default Client;
