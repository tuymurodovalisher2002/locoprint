import React, { useState } from "react";
import { Modal, Button } from "antd";
function ServiceCardItem(props) {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <div className="servic-item">
        <div>{props.icon}</div>
        <h3>{props.title}</h3>
        <p>{props.comment}</p>
        <Button type="primary" onClick={showModal}>{props.link}</Button>
      </div>
      <Modal
        title={props.title}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={handleOk}
      >
        <p>{props.comment}</p>
      </Modal>
    </>
  );
}

export default ServiceCardItem;
