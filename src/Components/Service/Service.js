import React, { useState } from "react";
import "./Service.scss";
import "./ServiceCard";
import { servicTitle } from "./ServiceCard";
import ServiceCardItem from "./ServiceCardItem";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "./Swiper.scss";
import { Autoplay, Pagination } from "swiper";
import { Fade } from "react-reveal";
import "./Responsive.scss";

function Service() {
  const [servicCards, setServicCards] = useState(servicTitle);
  return (
    <section className="service" id="service">
      <div className="container">
        <div className="servic-title">
          <Fade right>
            <h5>Подробнее о наших услугах</h5>
          </Fade>
          <Fade left delay={100}>
            <h1>Услуги компании</h1>
          </Fade>
        </div>
        <Swiper
          // slidesPerView={3}
          spaceBetween={30}
          autoplay={{
            delay: 5000,
            disableOnInteraction: false
          }}
          pagination={{
            clickable: true
          }}
          breakpoints={{
            300: { slidesPerView: 1 },
            768: {
              slidesPerView: 3
            }
          }}
          modules={[Autoplay, Pagination]}
          className="mySwiper"
        >
          {servicCards.map((card) => {
            return (
              <SwiperSlide>
                <div
                  className={
                    card.id === 1
                      ? "servic-card servic-card_active"
                      : "servic-card"
                  }
                >
                  <Fade bottom delay={100}>
                    <ServiceCardItem
                      key={card.id}
                      title={card.title}
                      comment={card.comment}
                      link={card.link}
                      icon={card.icon}
                    />
                  </Fade>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
      <div className="line"></div>
    </section>
  );
}

export default Service;
